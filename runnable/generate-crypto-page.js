import path from 'path'
import fs from 'fs-extra'

import CRYPTO from '../source/crypto.json'

function generateHTML(aspectRatioWidth, aspectRatioHeight) {
	let html = `
		<html>
			<title>Crypto</title>
			<body>
	`

	html += `
		<style>
			body {
				margin: 0;
				font-family: sans-serif;
			}

			.Countries {
				display: flex;
				flex-wrap: wrap;
			}

			.Country {
				flex: 0 0 10%;
				flex-direction: column;
				align-items: center;
				padding: 1em;
				box-sizing: border-box;
				overflow: hidden;
			}

			.CountryFlagContainer {
				position: relative;
				width: 100%;
				padding-bottom: calc(100% * ${aspectRatioHeight} / ${aspectRatioWidth});
			}

			.CountryFlag {
				position: absolute;
				width: 100%;
				height: 100%;
				box-shadow: 0 0 0 1px black;
			}

			.Country h1 {
				text-align: center;
				overflow: hidden;
				text-overflow: ellipsis;
			}
		</style>
	`

	html += '<div class="Countries">'

	for (const asset of CRYPTO) {
		html += `
			<section class="Country">
				<div class="CountryFlagContainer">
					<a href="https://www.google.com/search?q=${encodeURIComponent(asset + ' flag')}&tbm=isch" target="_blank" class="CountryFlagLink">
						<img title="${asset}" loading="lazy" class="CountryFlag" src="./${asset}.svg"/>
					</a>
				</div>
				<h1 title="${asset}">
					${asset}
				</h1>
			</section>
		`
	}

	html += `
				</div>
			</body>
		</html>
	`

	fs.outputFileSync(path.resolve(`./website/${aspectRatioWidth}x${aspectRatioHeight}/index.html`), html)
}

generateHTML(3, 2)
generateHTML(1, 1)