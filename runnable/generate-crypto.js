import path from 'path'
import fs from 'fs'

const crypto = []
fs.readdirSync(path.resolve('./crypto/svg')).map((filename) => {
	crypto.push(filename.slice(0, filename.indexOf('.svg')))
})

fs.writeFileSync(path.join('./source/crypto.json'), JSON.stringify(crypto))

// Stupid Node.js can't even `import` JSON files.
// https://stackoverflow.com/questions/72348042/typeerror-err-unknown-file-extension-unknown-file-extension-json-for-node
fs.writeFileSync(path.join('./source/crypto.json.js'), 'export default ' + JSON.stringify(crypto))